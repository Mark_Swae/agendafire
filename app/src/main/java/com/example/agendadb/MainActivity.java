package com.example.agendadb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agendadb.Objetos.Contactos;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText txtNombre, txtTelefono1, txtTelefono2, txtDireccion, txtNotas;
    private CheckBox checkFavorito;
    private Button btnLimpiar, btnGuardar, btnListar, btnSalir;
    private TextView lblNombre, lblTelefono1, lblDireccion;
    String id;

    DatabaseReference databaseReference;
    Contactos savedContacto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();
        iniciarFirebase();
    }

    // Iniciando componentes
    private void iniciarFirebase(){
        FirebaseApp.initializeApp(this);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Agenda").child("Contacto");
    }

    public void iniciarComponentes(){
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtDireccion = (EditText) findViewById(R.id.txtDireccion);
        txtNotas = (EditText) findViewById(R.id.txtNotas);
        txtTelefono1 = (EditText) findViewById(R.id.txtTelefono1);
        txtTelefono2 = (EditText) findViewById(R.id.txtTelefono2);

        checkFavorito = (CheckBox) findViewById(R.id.cbxFavorito);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        lblDireccion = (TextView) findViewById(R.id.lblDireccion);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        lblTelefono1 = (TextView) findViewById(R.id.lblTelefono1);
        otorgarAccion();

    }

    public void otorgarAccion(){
        btnLimpiar.setOnClickListener(this);
        btnGuardar.setOnClickListener(this);
        btnListar.setOnClickListener(this);
        btnSalir.setOnClickListener(this);
    }


    // Métodos
    public void limpiar(){
        txtTelefono1.setText("");
        txtDireccion.setText("");
        txtNombre.setText("");
        txtTelefono1.setText("");
        txtNotas.setText("");
        txtTelefono2.setText("");
        checkFavorito.setChecked(false);

        txtNombre.requestFocus();
    }

    public void guardar(){
        if(validarCajas()){
            if(savedContacto == null){
                insertarContacto();
                limpiar();
                Toast.makeText(this, "Contacto agregado con éxito", Toast.LENGTH_LONG).show();
            }else{
                actualizarContacto(id, savedContacto);
                limpiar();
                Toast.makeText(this, "Contacto actualizado con éxito", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "Campos vacíos", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean validarCajas(){
        if(txtNombre.getText().toString().matches("")){
            lblNombre.setText("Nombre *Campo requerido*");
            lblTelefono1.setText("Teléfono");
            this.lblDireccion.setText("Domicilio");
            this.txtNombre.requestFocus();
            return false;
        }else if(txtTelefono1.getText().toString().matches("")){
            lblNombre.setText("Nombre");
            lblTelefono1.setText("Teléfono *Campo requerido*");
            this.lblDireccion.setText("Domicilio");
            this.txtTelefono1.requestFocus();
            return false;
        } else if(txtDireccion.getText().toString().matches("")){
            lblNombre.setText("Nombre");
            this.lblDireccion.setText("Domicilio *Campo requerido*");
            lblTelefono1.setText("Teléfono");
            this.txtDireccion.requestFocus();
            return false;
        }else{
            return true;
        }
    }

    public void insertarContacto(){
        Contactos contacto = new Contactos();
        contacto.set_ID(UUID.randomUUID().toString());
        contacto.setDireccion(this.txtDireccion.getText().toString());
        contacto.setNombre(this.txtNombre.getText().toString());
        contacto.setNotas(this.txtNotas.getText().toString());
        contacto.setTelefono1(this.txtTelefono1.getText().toString());
        contacto.setTelefono2(this.txtTelefono2.getText().toString());
        if(this.checkFavorito.isChecked()){
            contacto.setFavorite(1);
        }else{
            contacto.setFavorite(0);
        }

        databaseReference.child(contacto.get_ID()).setValue(contacto);
    }

    public void actualizarContacto(String id, Contactos contacto) {
        contacto.set_ID(id);
        contacto.setDireccion(this.txtDireccion.getText().toString());
        contacto.setNombre(this.txtNombre.getText().toString());
        contacto.setNotas(this.txtNotas.getText().toString());
        contacto.setTelefono1(this.txtTelefono1.getText().toString());
        contacto.setTelefono2(this.txtTelefono2.getText().toString());
        if(this.checkFavorito.isChecked()){
            contacto.setFavorite(1);
        }else{
            contacto.setFavorite(0);
        }
        databaseReference.child(String.valueOf(id)).setValue(contacto);
    }

    public void listar(){
        Intent i= new Intent(MainActivity.this,ListaActivity.class);
        limpiar();
        startActivityForResult(i,0);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            if(Activity.RESULT_OK == resultCode){
                Contactos contacto = (Contactos) oBundle.getSerializable("contacto");
                savedContacto = contacto;
                id = contacto.get_ID();
                txtNombre.setText(contacto.getNombre());
                txtTelefono1.setText(contacto.getTelefono1());
                txtTelefono2.setText(contacto.getTelefono2());
                txtDireccion.setText(contacto.getDireccion());
                txtNotas.setText(contacto.getNotas());
                if(contacto.getFavorite()>0){checkFavorito.setChecked(true);}
            }else{
                limpiar(); }
        }
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnGuardar:
                guardar();
                break;
            case R.id.btnLimpiar:
                Toast.makeText(this, "Listo", Toast.LENGTH_SHORT).show();
                limpiar();
                break;
            case R.id.btnListar:
                listar();
                break;
            case R.id.btnSalir:
                finish();
                break;
        }
    }
}